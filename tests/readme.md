tests
===========

1. Update role
2. rename role in `tests/roles`
3. Update Vagrantfile, if needed
3. Update role name in `tests/test.yml`
3. Run `vagrant up`


Notes
------

If you change the metadata of the role, especilly the platform part, ensure that the vm box used by vagrant matches.

The `ansible.cfg` file makes it possible to run e.g. `ansible-playbook` ignoring vagrant. This is relevant for debugging.
